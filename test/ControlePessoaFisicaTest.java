/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import controller.ControlePessoaFisica;
import controller.ControlePessoaJuridica;
import model.PessoaFisica;
import model.PessoaJuridica;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author reis
 */
public class ControlePessoaFisicaTest {
    
    PessoaFisica umaPessoa;
    ControlePessoaFisica umControle;
    PessoaFisica pessoaPesquisada;
    @Before
    public void setUp() throws Exception{
        umaPessoa = new PessoaFisica("João", "333-333", "4444-4444", "000.000.000");
        umControle = new ControlePessoaFisica();
        umControle.adicionaPessoaFisica(umaPessoa);
    }
    
    @Test
    public void pesquisarPessoaJuridicaTest () {
        pessoaPesquisada = umControle.pesquisaPessoaFisica(umaPessoa.getNome());
        assertNotNull(pessoaPesquisada);
    }
    
    @Test
    public void removerPessoaJuridicaTest () {
        umControle.removePessoaFisica(umaPessoa);
        pessoaPesquisada = umControle.pesquisaPessoaFisica(umaPessoa.getNome());
        assertNull(pessoaPesquisada);
    }
}
