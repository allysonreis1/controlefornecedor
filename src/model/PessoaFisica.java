
package model;

public class PessoaFisica extends Fornecedor{
    private String cpf;
	
    public PessoaFisica (String nome, String telefoneFixo, String telefoneCelular, String cpf) {
	super(nome, telefoneFixo, telefoneCelular);
	this.cpf = cpf;
    }
	
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
}
